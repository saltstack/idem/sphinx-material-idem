# Fonts from builddocs
mkdir -p /usr/share/fonts/truetype
mkdir -p /usr/share/fonts/opentype
cp -rfv builddocs/builddocs/files/fonts/truetype/* /usr/share/fonts/truetype/
cp -rfv builddocs/builddocs/files/fonts/opentype/* /usr/share/fonts/opentype/
fc-cache -f -v

# sphinx-tabs formatting workaround
## Replacing tabs with note admonitions, to properly render
for TAB_FILE in $(grep -R '.. tabs::' docs/ | cut -d':' -f1 | sort | uniq)
do
    sed -i 's/\ \ \ \.\.\ tabs:://g' $TAB_FILE
    sed -i 's/\ \ \ \ \.\.\ tab::/\ \ \.\. admonition::/g' $TAB_FILE
done
